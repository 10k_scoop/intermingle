import React from "react";
import { Platform, StatusBar, StyleSheet, Text, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { FontAwesome } from "@expo/vector-icons";
import { RFValue as rf } from "react-native-responsive-fontsize";
export default function Header() {
  return (
    <View style={styles.cont}>
      <View style={styles.header}>
        <View style={styles.leftSideHeader}>
          <FontAwesome name="bars" size={24} color="#F68724" />
          <Text style={{ fontSize: rf(13), marginLeft: 10 }}>Home</Text>
        </View>
        <View style={styles.rightSideHeader}>
          <Text style={{ fontSize: rf(10), textAlign: "right" }}>
            Welcome Shabbir Hussain{"\n"}
            September 20,201
          </Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    width: wp("100%"),
    height: hp("8%"),
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: wp("5%"),
    marginTop:
      Platform.OS == "android"
        ? StatusBar.currentHeight + 10
        : StatusBar.currentHeight + hp("2%"),
  },
  cont: {
    backgroundColor: "#E3EAF4",
  },
  leftSideHeader: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
});
