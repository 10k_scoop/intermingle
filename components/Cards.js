import React from "react";
import { TouchableOpacity, StyleSheet, Text, View } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  AntDesign,
  Zocial,
  Foundation,
  Ionicons,
  Feather,
} from "@expo/vector-icons";
export default function Cards(props) {
  return (
    <View style={styles.cont}>
      <View style={styles.row2}>
        <Text style={[styles.detailText, { fontSize: rf(14) }]}>
          Mr. Requester
        </Text>
        <Text
          style={[styles.detailText, { fontWeight: "700", fontSize: rf(14) }]}
        >
          Please Review
        </Text>
      </View>
      <View style={styles.row2}>
        <Text style={styles.detailText}>RQ# 1000827</Text>
        <Text style={[styles.detailText, { fontWeight: "300" }]}>
          09/09/2021
        </Text>
      </View>
      <View style={styles.row2}>
        <Text style={styles.detailText}>
          <Text
            style={{ fontSize: rf(11), fontWeight: "700", fontSize: rf(12) }}
          >
            Vendor:
          </Text>
          Xiamen sanan Integrated Circuit Co.,Ltd
        </Text>
        <Text style={styles.detailText}>$3,400.00</Text>
      </View>

      <View style={styles.actionBtnsWrapper}>
        <TouchableOpacity style={styles.actionBtn1}>
          <Zocial name="email" size={22} color="white" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.actionBtn1}>
          <Foundation name="photo" size={22} color="white" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.actionBtn1}>
          <Ionicons name="chatbubble-ellipses" size={22} color="white" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.actionBtn1}>
          <Feather name="check-square" size={22} color="white" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.actionBtn}>
          <AntDesign name="edit" size={22} color="white" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.actionBtn}>
          <AntDesign name="printer" size={22} color="white" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.actionBtn}>
          <AntDesign name="delete" size={22} color="white" />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  cont: {
    width: wp("95%"),
    height: hp("25%"),
    backgroundColor: "#E3EAF4",
    borderRadius: 10,
    padding: 10,
    marginBottom: 10,
  },
  row1: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    marginBottom: hp("2%"),
  },
  colWrapper: {
    height: hp("2.5%"),
    backgroundColor: "#AEADAD",
    borderRadius: 100,
    paddingHorizontal: wp("4%"),
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",

    marginRight: wp("2%"),
  },
  row1Text: {
    color: "#fff",
    fontSize: rf(10),
  },
  priortyIndicator: {
    width: 7,
    height: 7,
    backgroundColor: "lightgreen",
    borderRadius: 100,
    left: 5,
  },
  row2: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 5,
    marginVertical: 2,
  },
  detailText: {
    fontSize: rf(11),
  },
  actionBtnsWrapper: {
    alignItems: "flex-end",
    flexDirection: "row",
    flex: 1,
  },
  actionBtn: {
    width: wp("10%"),
    height: wp("10%"),
    backgroundColor: "grey",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: wp("10%"),
    marginHorizontal: 3,
  },
  btnText: {
    fontWeight: "bold",
    color: "white",
    fontSize: rf(13),
  },
  actionBtn1: {
    width: wp("10%"),
    height: hp("5%"),
    backgroundColor: "#4f5d74",
    borderRadius: 6,
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: 3,
  },
});
