import React from "react";
import {
    StyleSheet,
    Text,
    View,
} from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { TouchableOpacity } from "react-native-gesture-handler";
export default function BotomSaveBtn({Onpres}) {
    return (

        <TouchableOpacity style={styles.container} onPress={Onpres}>
            <Text style={{ fontSize: rf(14), color: '#fff', fontWeight: '700' }}>Save</Text>
        </TouchableOpacity>

    );
}

const styles = StyleSheet.create({
    container: {
        height: hp('5%'),
        width: wp('40%'),
        borderRadius: 5,
        backgroundColor: '#FF9900',
        justifyContent:'center',
        alignItems:'center'
    }





});
