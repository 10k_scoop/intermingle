import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { MaterialIcons, MaterialCommunityIcons } from "@expo/vector-icons";
import { RFValue as rf } from "react-native-responsive-fontsize";
export default function ActionItem(props) {
  return (
    <TouchableOpacity style={styles.actionItemWrapper}>
      <View style={styles.actionItem}>
        {props.iconName == "file-document-outline" ? (
          <MaterialCommunityIcons
            name="file-document-outline"
            size={rf(18)}
            color="black"
          />
        ) : (
          <MaterialIcons name={props.iconName} size={rf(18)} color="black" />
        )}
        <Text style={styles.screenText}>{props.title}</Text>
        <Text style={[styles.screenText, { flex: 0, paddingHorizontal: 10 }]}>
          {props.number}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  actionItem: {
    width: "100%",
    backgroundColor: "#e3eaf4",
    height: hp("6%"),
    paddingHorizontal: wp("3%"),
    justifyContent: "center",
    flexDirection: "row",
    borderRadius: 10,
    alignItems: "center",
  },
  actionItemWrapper: {
    width: wp("95%"),
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: wp("2.5%"),
    marginTop: hp("1%"),
  },
  screenText: {
    fontWeight: "bold",
    fontSize: rf(12),
    flex: 1,
    marginLeft: 10,
  },
});
